FROM node:17-buster AS builder

ENV NODE_OPTIONS='--max_old_space_size=4096'
ENV PORT=8000

RUN apt update
RUN apt install -y gettext
RUN yarn add global ts-node nodemon

WORKDIR /app/
COPY ./ ./
CMD ["yarn" , "run", "start"]